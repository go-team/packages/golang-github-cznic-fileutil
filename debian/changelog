golang-github-cznic-fileutil (0.0~git20200808.2079183-4) unstable; urgency=medium

  * Team upload.
  * Update import check path.
    - fixes build of golang-github-cznic-b (Closes: #980379).
    - fixes build of golang-github-cznic-ql (Closes: #980572).
    - fixes build of golang-github-cznic-lldb (Closes: #980668).

 -- Aloïs Micard <alois@micard.lu>  Mon, 08 Feb 2021 09:33:37 +0100

golang-github-cznic-fileutil (0.0~git20200808.2079183-3) unstable; urgency=medium

  * Team upload.
  * Repair broken import check patch.

 -- Alexandre Viau <aviau@debian.org>  Tue, 29 Dec 2020 02:22:30 -0500

golang-github-cznic-fileutil (0.0~git20200808.2079183-2) unstable; urgency=medium

  * Team upload.
  * Create remove-import-check.patch.

 -- Alexandre Viau <aviau@debian.org>  Tue, 29 Dec 2020 02:13:38 -0500

golang-github-cznic-fileutil (0.0~git20200808.2079183-1) unstable; urgency=medium

  * Team upload.
  * Point to new VCS.
  * Support new import path.
  * Create mathutil-old-import.patch.
  * Rules-Requires-Root: no.

 -- Alexandre Viau <aviau@debian.org>  Tue, 29 Dec 2020 01:25:07 -0500

golang-github-cznic-fileutil (0.0~git20181122.4d67cfe-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 0.0~git20181122.4d67cfe
    - Remove patch, has been applied upstream
    - FTBFS: go 1.13 changed package import behaviour, causing
      test flags to be parsed before they were declared
  * Update team name
  * Update to Standards-Version 4.4.1
    - Use HTTPS URL for d/copyright
  * Use golang-any instead of golang-go
  * Switch to debhelper-compat and use v12
  * Add d/watch for git mode
  * Adapt gbp.conf

 -- Dr. Tobias Quathamer <toddy@debian.org>  Tue, 15 Oct 2019 12:08:48 +0200

golang-github-cznic-fileutil (0.0~git20150708.0.1c9c88f-4) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Roger Shimizu ]
  * debian/control:
    - Bump Priority from extra to optional.
    - Add autopkgtest-pkg-go Testsuite.

 -- Roger Shimizu <rogershimizu@gmail.com>  Fri, 11 Jan 2019 00:40:11 +0200

golang-github-cznic-fileutil (0.0~git20150708.0.1c9c88f-3) unstable; urgency=medium

  * Team upload.
  * debian/control:
    - Fix git repo path in Vcs-Git.

 -- Roger Shimizu <rogershimizu@gmail.com>  Sun, 09 Jul 2017 14:03:12 +0900

golang-github-cznic-fileutil (0.0~git20150708.0.1c9c88f-2) unstable; urgency=medium

  * Team upload.

  [ Tim Potter ]
  * debian/control:
    - Add me to uploaders.

  [ Roger Shimizu ]
  * debian/patches:
    - Add a patch from upstream to fix FTBFS on 32-bit system.
      (Closes: #860660).

 -- Roger Shimizu <rogershimizu@gmail.com>  Thu, 04 May 2017 13:03:57 +0900

golang-github-cznic-fileutil (0.0~git20150708.0.1c9c88f-1) unstable; urgency=medium

  * Initial release (Closes: #813950).

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 05 Nov 2015 00:47:03 +1100
